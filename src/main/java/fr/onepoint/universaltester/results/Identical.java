package fr.onepoint.universaltester.results;

public class Identical extends Result {

    public Identical(String filename) {
        super(filename, Status.OK);
    }
}
