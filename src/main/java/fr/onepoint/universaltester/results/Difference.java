package fr.onepoint.universaltester.results;

public class Difference extends Result{

    private final String actual;
    private final String expected;
    private final String message;

    public Difference(String message, String actual, String expected, String filename) {
        super(filename, Status.KO);
        this.actual = actual;
        this.expected = expected;
        this.message = message;
    }

    public String getActual() {
        return actual;
    }

    public String getExpected() {
        return expected;
    }

    public String getMessage() {
        return message;
    }
}
