package fr.onepoint.universaltester.results;

public abstract class Result {

    private String filename;
    private Status status;

    public Result(String filename, Status status){
        this.filename = filename;
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public Status getStatus(){
        return status;
    }
}
