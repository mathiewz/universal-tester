package fr.onepoint.universaltester.results;

public class Failure extends Result{

    private final Throwable throwable;

    public Failure(String caseName, Throwable throwable) {
        super(caseName, Status.ERROR);
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
