package fr.onepoint.universaltester;

import fr.onepoint.universaltester.executor.HasMandatoryProps;
import fr.onepoint.universaltester.executor.Reporter;
import fr.onepoint.universaltester.executor.Runner;
import fr.onepoint.universaltester.executor.Tester;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.text.MessageFormat.format;

public class TestCaseBuilder {

    private static final List<String> mandatoryProperties = Arrays.asList(
            TestCase.ASSERTIONS_DIRECTORY,
            TestCase.SSH_HOST,
            TestCase.SSH_USER,
            TestCase.SSH_PASSWORD,
            TestCase.SSH_PORT,
            TestCase.CASE_NAME
    );

    private Config config;

    private List<Runner> runners;

    private List<Tester> testers;

    private List<Reporter> reporters;

    private final List<String> propertiesToCheck;

    public TestCaseBuilder(){
        config = new Config();
        runners = new ArrayList<>();
        testers = new ArrayList<>();
        reporters = new ArrayList<>();
        propertiesToCheck = new ArrayList<>(mandatoryProperties);
    }

    public TestCaseBuilder addRunner(Runner runner){
        if(runner instanceof HasMandatoryProps){
            propertiesToCheck.addAll(((HasMandatoryProps) runner).getMandatoryProperties());
        }
        runners.add(runner);
        return this;
    }

    public TestCaseBuilder addConfig(Path configLocation){
        config.addProperties(configLocation);
        return this;
    }

    public TestCaseBuilder addConfig(Config config){
        this.config.addConfig(config);
        return this;
    }

    public TestCaseBuilder addProperty(String name, String value){
        config.addProperty(name, value);
        return this;
    }

    public TestCaseBuilder addTester(Tester tester){
        if(tester instanceof HasMandatoryProps){
            propertiesToCheck.addAll(((HasMandatoryProps) tester).getMandatoryProperties());
        }
        testers.add(tester);
        return this;
    }

    public TestCaseBuilder addReporter(Reporter reporter){
        if(reporter instanceof HasMandatoryProps){
            propertiesToCheck.addAll(((HasMandatoryProps) reporter).getMandatoryProperties());
        }
        reporters.add(reporter);
        return this;
    }

    public TestCase build(){
        if(runners.isEmpty()){
            throw new IllegalArgumentException("Runners undefined");
        }
        if(testers.isEmpty()){
            throw new IllegalArgumentException("Testers undefined");
        }
        if(reporters.isEmpty()){
            throw new IllegalArgumentException("Reporters undefined");
        }

        for(String prop : propertiesToCheck){
            if(config.getProperty(prop) == null){
                throw new IllegalArgumentException(format("Mandatory property {0} undefined", prop));
            }
        }
        return new TestCase(config, runners, testers, reporters);
    }
}
