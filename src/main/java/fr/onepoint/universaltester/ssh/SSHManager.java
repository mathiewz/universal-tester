package fr.onepoint.universaltester.ssh;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class SSHManager {

    private SSHManager(){
        //To avoid instantiation
    }

    private static Map<String, CloseableSSH> sshByHost = new HashMap<>();

    /**
     * Allows to get already opened SSH connection to the specified target
     * @param host the host of the target machine
     * @param user the user used to connect
     * @param port the target port
     * @return An optional valued with the connection if it exists, an empty optional otherwise
     */
    public static Optional<SSH> getSSH(String host, String user, int port){
        String key = getSSHKey(host, user, port);
        return Optional.ofNullable(sshByHost.get(key));
    }

    /**
     * Allows to get and manage an SSH connection to the specified target.
     * If there is already a connection matching the credentials, it will be returned.
     * A new SSH connection will be created otherwise.
     * @param host the host of the target machine
     * @param user the user used to connect
     * @param password the password to use to connect
     * @param port the target port
     * @return An ssh connection matching the credentials.
     */
    public static SSH getSSH(String host, String user, String password, int port) {
        Optional<SSH> ssh = getSSH(host,user,port);
        if(ssh.isPresent()){
            return ssh.get();
        }
        CloseableSSH newSSh = new CloseableSSH(host, user, password, port);
        sshByHost.put(getSSHKey(host, user, port), newSSh);
        return newSSh;
    }

    private static String getSSHKey(String host, String user, int port){
        return String.join(":", host, user, String.valueOf(port));
    }

    /**
     * Close specified ssh connection and remove it from manager.
     * Will do nothing if the connection doesn't exists
     * @param host the host of the ssh connection
     * @param user the user connected with
     * @param port the port connected into
     */
    public static void close(String host, String user, int port){
        String key = getSSHKey(host, user, port);
        CloseableSSH ssh = sshByHost.get(key);
        if(ssh != null){
            ssh.close();
            sshByHost.remove(key);
        }
    }


}
