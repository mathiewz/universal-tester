package fr.onepoint.universaltester.ssh;

import fr.onepoint.universaltester.UniversalTesterException;

import java.nio.file.Path;

public interface SSH {

    /**
     * Run to remote connection a command line.
     * @param command The command line to execute.
     * @return The message printed on standard output during execution.
     * @throws UniversalTesterException When an error occurred during execution.
     */
    String exec(String command);

    /**
     * Copy a file from remote connection to local file system.
     * @param distantLocation The path of the file on the remote file system. It can be relative or absolute.
     * @param targetDir The directory on the local file system to store the downloaded file.
     * @return The path of the copied file on the local file system.
     */
    Path downloadFile(String distantLocation, Path targetDir);

    /**
     * Copy a file frmo the local file system to the remote file system.
     * @param toUpload The file to upload.
     * @param distantRepo The path of the directory on the remote file system to store the uploaded file.
     */
    void upload(Path toUpload, String distantRepo);

}
