package fr.onepoint.universaltester;

import fr.onepoint.universaltester.executor.Reporter;
import fr.onepoint.universaltester.executor.Runner;
import fr.onepoint.universaltester.executor.Tester;
import fr.onepoint.universaltester.results.Failure;
import fr.onepoint.universaltester.results.Result;
import fr.onepoint.universaltester.ssh.SSH;
import fr.onepoint.universaltester.ssh.SSHManager;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static fr.onepoint.universaltester.ssh.SSHManager.getSSH;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.text.MessageFormat.format;

public class TestCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestCase.class);

    public static final String SSH_HOST = "ssh.host";
    public static final String SSH_USER = "ssh.user";
    public static final String SSH_PASSWORD = "ssh.password";
    public static final String SSH_PORT = "ssh.port";
    public static final String ASSERTIONS_DIRECTORY = "assertions.directory";
    public static final String CASE_NAME = "assertions.casename";

    private Config config;

    private List<Runner> runners;

    private List<Tester> testers;

    private List<Reporter> reporters;

    private String caseName;

    public TestCase(String caseName, Config config, List<Runner> runners, List<Tester> testers, List<Reporter> reporters) {
        this.caseName = caseName;
        this.config = config;
        this.runners = runners;
        this.testers = testers;
        this.reporters = reporters;
    }
    public TestCase(Config config, List<Runner> runners, List<Tester> testers, List<Reporter> reporters) {
        this(config.getProperty(CASE_NAME), config, runners, testers, reporters);
    }

    public void test(){
        String host = config.getProperty(SSH_HOST);
        String user = config.getProperty(SSH_USER);
        String pass = config.getProperty(SSH_PASSWORD);
        int port = Integer.parseInt(config.getProperty(SSH_PORT));

        reporters.forEach(report -> report.beforeCase(config));
        List<Result> results = new ArrayList<>();
        SSH ssh = getSSH(host, user, pass, port);
        try {
            runners.forEach(runner -> runner.run(config, ssh));
            for(Tester tester : testers){
                tester.prepareFiles(config, ssh).entrySet().stream()
                        .map(set -> getDistantFile(set.getKey(), set.getValue(), ssh))
                        .flatMap(path -> tester.compare(getRefPath(path), path, config).stream())
                        .forEach(results::add);
            }
        } catch (Exception e){
            LOGGER.debug("Exception occured during case {}", getCaseName(), e);
            results.add(new Failure(getCaseName(), e));
        } finally {
            SSHManager.close(host, user, port);
        }
        reporters.forEach(reporter -> reporter.report(results, config));
    }

    private Path getDistantFile(String newFileName, String fileDistantLocation, SSH connection) {
        Path target = Paths.get(config.getProperty(ASSERTIONS_DIRECTORY));
        Path copiedFile = connection.downloadFile(fileDistantLocation, target);
        Path newFile = copiedFile.getParent().resolve(format("{0}.{1}",  newFileName, FilenameUtils.getExtension(copiedFile.toFile().getName())));
        try {
            Files.move(copiedFile, newFile, REPLACE_EXISTING);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        LOGGER.debug("Rename file {} to {}", copiedFile, newFileName);
        checkRefFile(newFile);
        return newFile;
    }

    private void checkRefFile(Path copiedFile) {
        Path refFile = getRefPath(copiedFile);
        if(!refFile.toFile().exists()){
            try {
                Files.copy(copiedFile, refFile);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    private Path getRefPath(Path copiedFile) {
        Path dir = copiedFile.getParent();
        String filename = copiedFile.toFile().getName();
        return dir.resolve("ref_"+filename);
    }

    public List<Reporter> getReporters() {
        return reporters;
    }

    public String getCaseName() {
        return caseName;
    }
}
