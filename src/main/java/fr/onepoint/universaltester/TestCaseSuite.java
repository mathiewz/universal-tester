package fr.onepoint.universaltester;

import fr.onepoint.universaltester.executor.Reporter;

import java.util.List;
import java.util.stream.Collectors;

public class TestCaseSuite {

    private List<TestCase> testCases;

    public TestCaseSuite(List<TestCase> testCases){
        this.testCases = testCases;
    }

    public void test(){
        List<Reporter> reporters = testCases.stream()
                .map(TestCase::getReporters)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());

        reporters.forEach(Reporter::beforeCaseSuite);
        testCases.forEach(TestCase::test);
        reporters.forEach(Reporter::afterCaseSuite);
    }
}

