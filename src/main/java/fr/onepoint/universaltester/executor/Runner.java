package fr.onepoint.universaltester.executor;

import fr.onepoint.universaltester.Config;
import fr.onepoint.universaltester.ssh.SSH;

/**
 * First step : execute main process of testing : run the operation in distant server
 */
@FunctionalInterface
public interface Runner {

    void run(Config config, SSH ssh);

}
