package fr.onepoint.universaltester.executor;

import fr.onepoint.universaltester.Config;
import fr.onepoint.universaltester.results.Result;
import fr.onepoint.universaltester.ssh.SSH;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Second step : prepare files to be compared, and return their absolute path on the server, or the relative path from home folder of the ssh user
 */
public interface Tester {
    Map<String, String> prepareFiles(Config config, SSH ssh);

    List<Result> compare(Path reference, Path toCompare, Config config);
}
