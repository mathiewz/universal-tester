package fr.onepoint.universaltester.executor;

import fr.onepoint.universaltester.Config;
import fr.onepoint.universaltester.results.Result;

import java.util.List;

/**
 * Report the results of the test
 */
@FunctionalInterface
public interface Reporter {

    default void beforeCaseSuite(){}

    default void beforeCase(Config config){}

    void report(List<Result> results, Config config);

    default void afterCaseSuite(){}

}
