package fr.onepoint.universaltester.executor;

import java.util.List;

public interface HasMandatoryProps {

    List<String> getMandatoryProperties();

}
