package fr.onepoint.universaltester;

public class UniversalTesterException extends RuntimeException {

    public UniversalTesterException(Exception cause){
        super(cause);
    }

}
